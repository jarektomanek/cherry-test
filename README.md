# Games Lobby - Jarek's assessment test

## Installation

1. ``` bower install ```

2. ``` npm install ```

## Building

### For development (linting, no JS & CSS minification etc., templates uncached, source maps, watcher, live reload active)
``` grunt ```

### For production (linting, JS minification and concatenation, mangling, templates to JS)
``` grunt --env=production ```

## Unit test
Not integrated with Grunt. Correctly generated only in development environment. Open runner in a browser:

[/SpecRunner.html](http://jarektomanek.bitbucket.org/SpecRunner.html) (in build directory)

## Demo
[http://jarektomanek.bitbucket.org/](http://jarektomanek.bitbucket.org/)