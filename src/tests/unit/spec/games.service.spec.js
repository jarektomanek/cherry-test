(function(){
    'use strict';

    describe('gamesService', function () {
		var gamesService;
        var $httpBackend;
        var mockGameResponse;

        beforeEach(module('app'));

        beforeEach(inject(function ($injector) {
            gamesService = $injector.get('gamesService');
            $httpBackend = $injector.get('$httpBackend');
            mockGameResponse = {
                "background": "https://d32fpk1qdfr4aq.cloudfront.net/img/games/gamebg/gonzos-quest1381304301.jpg?u=1455815690",
                "cols": null,
                "description": "",
                "game_stakes": null,
                "height": "480",
                "label": null,
                "name": "Gonzo's Quest",
                "rows": null,
                "slug": "gonzos-quest",
                "thumbnail": "https://d32fpk1qdfr4aq.cloudfront.net/img/games/150x140/gonzos-quest.jpg?u=1455815690",
                "vendor": "netent",
                "vendor_id": "gonzos-quest.desktop",
                "width": "640",
                "id": "netent-gonzos-quest",
                "url": "sessId=DEMO-170120bbaf-EUR",
                "created_at": {
                    "date": "2013-08-06 14:01:58.000000",
                    "timezone_type": 1,
                    "timezone": "+00:00"
                },
                "sq_thumbnail": null,
                "screenshot": null,
                "homepage_image": null,
                "meta": {
                    "description_short": "",
                    "currency": "EUR",
                    "description_long": "<p>A <strong>video slot</strong> which has the makings of a truly golden adventure. <strong>Gonzo</strong>, our conquistador, is about to set off to find the lost city of gold and assist you with your own quest... so get those Avalanche symbols moving, as the Free Falls are what you need to unlock your own gold!</p>\n\n<p><strong>Gonzo&rsquo;s Quest</strong> is a five reel, 20 payline <strong>video</strong> <strong>slot machine</strong> that takes you on an adventure through South America to discover the ancient Inca riches. As you follow Gonzo, a short but fierce animated character based on the legendary Spanish conquistador Gonzalo Pizarro, you&rsquo;ll experience many exciting features. First and foremost is the avalanche feature, a feature unique to Net Entertainment that makes the symbols appear to fall down the reels rather than spin. <strong>Gonzo&rsquo;s Quest </strong>was one of the first <strong>video</strong> <strong>slot machines</strong> to implement this feature, and it has since become quite popular amongst players. This is simply because the feature is beneficial, since whenever a win is achieved, the symbols will disappear from the reels, allowing more symbols to fall into their place with the potential for another win without having to place an additional bet.</p>\n\n<p>On top of this, <strong>Gonzo&rsquo;s Quest </strong>is packed with a wide variety of bonus features to keep you interested throughout the game.</p>\n\n<p>Free spins bonus round: Whenever three, four or five free fall symbols appear on a winning payline, you&rsquo;ll trigger the free spins bonus round. Here, the symbols will free fall multiple times, giving you free chances to win. Any win achieved from a free spin will also be awarded a multiplier of up to 15 times, giving you the chance to take your prize to the next level.</p>\n\n<p>Wilds: Wild symbols are important to look out for in this <strong>video</strong> <strong>slot machine</strong> &ndash; as well as finishing your winning paylines, they&rsquo;ll also help you to trigger extra free spins.&nbsp;</p>\n",
                    "max_bet": 50,
                    "bonus": false,
                    "lines": 20
                },
                "enabled": true,
                "volatility": 0,
                "rating": 0,
                "backgrounds": [
                    "https://d32fpk1qdfr4aq.cloudfront.net/media/games/netent/eldorado_sw/netent-eldorado_sw-B470x920L.jpg?u=1455815690",
                    "https://d32fpk1qdfr4aq.cloudfront.net/media/games/netent/eldorado_sw/netent-eldorado_sw-B470x920R.jpg?u=1455815690"
                ],
                "screenshots": [],
                "thumbnails": {
                    "280x280": "https://d32fpk1qdfr4aq.cloudfront.net/media/games/netent/eldorado_sw/netent-eldorado_sw-T280x280.jpg?u=1455815690",
                    "280x600": "https://d32fpk1qdfr4aq.cloudfront.net/media/games/netent/eldorado_sw/netent-eldorado_sw-T280x600.jpg?u=1455815690",
                    "600x280": "https://d32fpk1qdfr4aq.cloudfront.net/media/games/netent/eldorado_sw/netent-eldorado_sw-T600x280.jpg?u=1455815690",
                    "legacy": "https://d32fpk1qdfr4aq.cloudfront.net/img/games/150x140/gonzos-quest.jpg?u=1455815690"
                },
                "pos_x": null,
                "pos_y": null,
                "_links": {
                    "self": {
                        "href": "https://staging-frontapi.cherrytech.com/games/netent-gonzos-quest"
                    }
                }
            };

        }));

        it('should be defined', function (){
			expect(gamesService).toBeDefined();
            expect(gamesService).toEqual(jasmine.any(Object));
        });

		describe('cacheGame', function (){
			it('should be defined', function (){
				expect(gamesService.cacheGame).toBeDefined();
			});


			it('should add a correct game data to the games list', function (){
                var gameData = angular.copy(mockGameResponse);
                gamesService.cacheGame(gameData);
				expect(gamesService.games[gameData.id]).toEqual(gameData);
			});

            it('should NOT add an incorrect game data to the games list', function (){
                var gameData = angular.copy(mockGameResponse);
                delete gameData.id;
                gamesService.cacheGame(gameData);
				expect(gamesService.games[gameData.id]).not.toBeDefined();
			});

        });



    });

})();
