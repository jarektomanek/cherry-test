;(function(){
    'use strict';

    angular
        .module('app')
        .service('gameCategoriesService', gameCategoriesService);

    gameCategoriesService.$inject = ['$cachedResource', '$q'];

    function gameCategoriesService($cachedResource, $q){

        // Attaching injected dependencies to the instance
        this.$cachedResource = $cachedResource;
        this.$q = $q;

        this.gameCategories = {};
        this.gameCategoryResource = this.instantiateGameCategoryResource();
    }


    // Function declarations (hoistable)
    ////////////////////////////////////////////////////////////////////////

    /**
	 * Builds a $resource for the game categories
	 *
     * @returns {undefined}
	 */
    gameCategoriesService.prototype.instantiateGameCategoryResource = function() {
        return this.$cachedResource(
            'categories',
            '/mockData/game-categories/:gameCategorySlug.json',
            // 'https://staging-frontapi.cherrytech.com/game-categories/:gameCategorySlug',
            {},
            {
                get: {
                    method: 'GET',
                    // Adding headers is moved to interceptor
                    // headers: {
                    //     'CherryTech-Brand': 'cherrycasino.desktop',
                    //     'Accept-Language': 'en-GB'
                    // },
                    cache: true
                }
            }
        );
    };

    /**
	 * Fetches all categories from the API
	 *
	 * @returns {object} Promise to the categories request
	 */
    gameCategoriesService.prototype.getAll = function(){

        // Return if cached
        if(Object.keys(this.gameCategories).length) {
            return this.$q.when(this.gameCategories);
        }

        return this.gameCategoryResource.get().$promise
            .then(this.getAllComplete.bind(this))
            .catch(this.getAllFailed.bind(this));
    };

    /**
	 * Handles correct response of getAll method.
     * Performs a basic data validation.
     * Requests to stores fetched data.
	 *
	 * @param {object} response
	 * @returns {object} Fetched categories
	 */
    gameCategoriesService.prototype.getAllComplete = function(response){
        // Response data structure validation
        if(!response || !response._embedded || !response._embedded.game_categories) {
            this.getAllFailed('Incorrect response data in XHR', response);
            return {};
        }

        response._embedded.game_categories.forEach(this.cacheCategory, this);

        return this.gameCategories;
    };

    /**
	 * Handles incorrect response of getAll method
	 *
	 * @param {object} error
	 * @returns {undefined}
	 */
    gameCategoriesService.prototype.getAllFailed = function(error){
        console.error('XHR Failed', error);
    };


    /**
	 * Stores category data for later use
	 *
	 * @param {object} gameCategory Category data
	 * @returns {undefined}
	 */
    gameCategoriesService.prototype.cacheCategory = function(gameCategory){
        if(!gameCategory || !gameCategory.slug) {
            console.error('Failed to cache category from the data given', gameCategory);
            return;
        }
        this.gameCategories[gameCategory.slug] = gameCategory;
    };

    /**
	 * Fetches one category by its slug
	 *
	 * @param {string} gameCategorySlug slug
	 * @returns {object} Promise to the category request
	 */
    gameCategoriesService.prototype.getBySlug = function(gameCategorySlug){

        if(this.gameCategories[gameCategorySlug]) {
            return this.$q.when(this.gameCategories[gameCategorySlug]);
        }

        return this.gameCategoryResource.get({gameCategorySlug: gameCategorySlug}).$promise
            .then(this.getOneComplete.bind(this))
            .catch(this.getOneFailed.bind(this));
    };

    /**
	 * Handles correct response of getBySlug method.
     * Performs a basic data validation.
     * Requests to stores fetched data.
	 *
	 * @param {object} response
	 * @returns {object} Fetched categories
	 */
    gameCategoriesService.prototype.getOneComplete = function(response){
        // Response data structure validation
        if(!response || !response._embedded || !response._embedded.games) {
            this.getOneFailed('Incorrect response data in XHR', response);
            return {};
        }

        this.cacheCategory(response);

        return this.gameCategories[response.slug];
    };

    /**
     * Handles incorrect response of getBySlug method
     *
     * @param {object} error
     * @returns {undefined}
     */
    gameCategoriesService.prototype.getOneFailed = function(error){
        console.error('XHR Failed', error);
    };

})();
