;(function(){
    'use strict';

    angular
        .module('app')
        .service('gamesService', gamesService);

    gamesService.$inject = ['$cachedResource', '$q', 'gameCategoriesService'];

	function gamesService($cachedResource, $q, gameCategoriesService){

        // Attaching injected dependencies to the instance
        this.$cachedResource = $cachedResource;
        this.$q = $q;
        this.gameCategoriesService = gameCategoriesService;
        this.gameResource = this.instantiateGamesResource();

        this.games = {};


    }


    // Function declarations (hoistable)
    ////////////////////////////////////////////////////////////////////////

    gamesService.prototype.instantiateGamesResource = function(){
        return this.$cachedResource(
            'games',
            '/mockData/games.json',
            // 'https://staging-frontapi.cherrytech.com/games/:gameId',
            {},
            {
                get: {
                    method: 'GET',
                    // Adding headers is moved to interceptor
                    // headers: {
                    //     'CherryTech-Brand': 'cherrycasino.desktop',
                    //     'Accept-Language': 'en-GB'
                    // },
                    cache: true
                }
            }
        );
    };
    gamesService.prototype.getAll = function(gameId){
        var deferred = this.$q.defer();

        this.gameCategoriesService.getAll()
            .then(categoriesGetComplete.bind(this));

        function categoriesGetComplete(gameCategories){
            Object.keys(gameCategories).forEach(function(gamesCategorySlug){
                cacheCategoryGames.call(this, gameCategories[gamesCategorySlug]);
            }, this);

            deferred.resolve(this.games);
        }

        function cacheCategoryGames(gameCategory){
            if(!gameCategory._embedded || !gameCategory._embedded.games) {
                return;
            }

            gameCategory._embedded.games.forEach(this.cacheGame.bind(this));
        }

        return deferred.promise;
    };

    gamesService.prototype.getById = function(gameId){

        // Return if cached
        if(this.games[gameId]) {
            return this.$q.when(this.games[gameId]);
        }

        return this.gameResource.get({gameId: gameId}).$promise
            .then(this.getComplete.bind(this))
            .catch(this.getFailed.bind(this));
    };

    gamesService.prototype.getComplete = function(response){
        // Response data structure validation
        if(typeof response !== 'object' || !response.id) {
            this.getFailed('Incorrect response data in XHR', response);
            return {};
        }

        this.cacheGame(response);

        return this.games[response.id];
    };

    gamesService.prototype.getFailed = function(error){
        console.error('XHR Failed', error);
    };

    gamesService.prototype.cacheGame = function(game){
        if(!game || !game.id) {
            console.error('Failed to cache category from the data given', game);
            return;
        }
        this.games[game.id] = game;
    };

})();
