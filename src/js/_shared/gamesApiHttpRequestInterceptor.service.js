;(function(){
    'use strict';

    angular
        .module('app')
        .factory('gamesApiHttpRequestInterceptor', gamesApiHttpRequestInterceptor);

    function gamesApiHttpRequestInterceptor() {
        return {
            request: function (config) {
                // TODO skip when config.url is not matching API url
                config.headers['CherryTech-Brand'] = 'cherrycasino.desktop';
                config.headers['Accept-Language'] = 'en-GB';
                return config;
            }
        };
    }

})();
