;(function(){
    'use strict';

    angular.module('app.appFilters')
        .filter('sanitize', sanitizeFilter);

    sanitizeFilter.$inject = ['$sce'];

    function sanitizeFilter($sce) {
        return function(htmlCode){
            return $sce.trustAsHtml(htmlCode);
        };
    }

})();
