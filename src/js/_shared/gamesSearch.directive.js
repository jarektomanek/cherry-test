;(function(){
    'use strict';

    angular.module('app.appDirectives')
        .directive('gamesSearch', gamesSearchDirective);

    gamesSearchDirective.$inject = ['$rootScope', '$window', 'gamesService'];

    function gamesSearchDirective($rootScope, $window, gamesService) {

        return({
            restrict: 'A',
            controller: gamesSearchDirectiveController,
            controllerAs: 'gamesSearch',
            templateUrl: '/templates/gamesSearch.tpl.html',
            link: gamesSearchDirectiveLink
        });

        function gamesSearchDirectiveController(){
            var ctrl = this;

            // Fetch categories list
            gamesService.getAll()
            .then(function(games){
                // Convert object to array
                ctrl.games = Object.keys(games).map(function(key) {
                    return games[key];
                });
            });
        }

        function gamesSearchDirectiveLink($scope, $element, $attrs){

            // Pressing ESC closes search box
            angular.element($window).on('keyup', function(event) {
                if(event.keyCode === 27) {
                    $rootScope.$applyAsync(function(){
                        $rootScope.isSearchBoxVisible = false;
                    });
                }
            });

            // Unbinding keyup listener
            $scope.$on('$destroy', function unbindEscCloser(){
                angular.element($window).off('keyup');
            });

        }

    }


})();
