;(function(){
    'use strict';

    angular.module('app.appDirectives')
        .directive('categoriesBar', categoriesBarDirective);

    categoriesBarDirectiveController.$inject = ['gameCategoriesService'];

    function categoriesBarDirective() {

        return({
            restrict: 'A',
            controller: categoriesBarDirectiveController,
            controllerAs: 'categoriesBar',
            link: categoriesBarDirectiveLink,
            templateUrl: '/templates/categoriesBar.tpl.html'
        });

    }

    // Function declarations (hoistable)
    ////////////////////////////////////////////////////////////////////////

    function categoriesBarDirectiveController(gameCategoriesService){
        var ctrl = this;

        // Attaching injected dependencies to the instance
        ctrl.gameCategoriesService = gameCategoriesService;

        // Fetch categories list
        ctrl.gameCategoriesService.getAll()
            .then(function(gameCategories){
                // Convert object to array
                ctrl.gameCategories = Object.keys(gameCategories).map(function(key) {
                    return gameCategories[key];
                });
            });
    }

    function categoriesBarDirectiveLink($scope, $element, $attrs){
        $scope.categoriesBar.activeCategory = $attrs.categoriesBarActiveCategory;
    }

})();
