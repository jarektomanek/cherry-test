;(function(){
    'use strict';

    // Search results items animation
    angular
        .module('app')
        .animation('.gamesList__item', gamesListItemAnimation);

        gamesListItemAnimation.$inject = ['$animateCss'];

        function gamesListItemAnimation($animateCss) {
            return {
                enter: function(element, doneFn) {
                    return $animateCss(element, {
                        from: { opacity: 0 },
                        to: { opacity: 1 },
                        duration: 0.15
                    });
                },

                leave: function(element, doneFn) {
                    return $animateCss(element, {
                        from: { opacity: 1 },
                        to: { opacity: 0 },
                        duration: 0.15
                    });
                }

            };
        }

})();
