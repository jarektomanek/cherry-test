;(function(){
    'use strict';

    angular
        .module('app.home')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['gameCategoriesService', 'gamesService'];

    function HomeController(gameCategoriesService, gamesService){
        var vm = this;

        // Attaching injected dependencies to the instance
        vm.gameCategoriesService = gameCategoriesService;
        vm.gamesService = gamesService;

        vm.gameCategoriesService.getAll()
            .then(function(gameCategories){
                vm.gameCategories = gameCategories;
            });
    }

})();
