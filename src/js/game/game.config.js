;(function(){
    'use strict';

    angular
        .module('app.game', ['templates-dist'])
        .config(gameConfig);

    gameConfig.$inject = ['$stateProvider'];

    function gameConfig($stateProvider) {
        $stateProvider
            .state('root.gamesCategory.game', {
                url: '/:gameId',
                views: {
                    'body@root': {
                        templateUrl: 'templates/game/game.tpl.html',
                        controller: 'GameController as gameVm'
                    }
                },
                resolve:{
                    gameId: ['$stateParams', function($stateParams){
                        return $stateParams.gameId;
                    }]
                }
            });
    }

})();
