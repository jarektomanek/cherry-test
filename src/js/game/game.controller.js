;(function(){
    'use strict';

    angular
        .module('app.game')
        .controller('GameController', GameController);

    GameController.$inject = ['gamesService', 'activeGameCategorySlug', 'gameId'];

    function GameController(gamesService, activeGameCategorySlug, gameId){
        var vm = this;

        // Attaching injected dependencies to the instance
        vm.gamesService = gamesService;
        vm.activeGameCategorySlug = activeGameCategorySlug;
        vm.gameId = gameId;

        gamesService.getById(vm.gameId)
            .then(function(game){
                vm.game = game;
            });

    }


})();
