;(function(){
    'use strict';

    // Modules init
    ////////////////////////////////////////////////////////////////////////////
    angular.module('app.appDirectives', []);
    angular.module('app.appServices', []);
    angular.module('app.appFilters', []);
    angular
        .module('app', [
            // Vendor
            'ngResource',
            'ngCachedResource',
            'ngAnimate',
            'ui.router',

            // wrapper components
            'app.appDirectives',
            'app.appServices',
            'app.appFilters',

            // per-view modules
            'app.home',
            'app.gameCategory',
            'app.game'
        ])
        .config(appConfig)
        .run(appRun);


    // Config
    ////////////////////////////////////////////////////////////////////////////
    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider'];
    function appConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

        $httpProvider.interceptors.push('gamesApiHttpRequestInterceptor');

        // UI router config
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state('root', {
                abstract: true,
                templateUrl: 'templates/layout.tpl.html'
            });
    }


    // Run
    ////////////////////////////////////////////////////////////////////////////
    appRun.$inject = ['$rootScope'];
    function appRun($rootScope) {
        $rootScope.isSearchBoxVisible = false;
    }


})();
