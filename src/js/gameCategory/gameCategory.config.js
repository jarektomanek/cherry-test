;(function(){
    'use strict';

    angular
        .module('app.gameCategory', ['templates-dist'])
        .config(categoryConfig);

    categoryConfig.$inject = ['$stateProvider'];

    function categoryConfig($stateProvider) {
        $stateProvider
            .state('root.gamesCategory', {
                url: '/:gameCategorySlug',
                views: {
                    'body@root': {
                        templateUrl: 'templates/category/category.tpl.html',
                        controller: 'GameCategoryController as gamesCategoryVm'
                    }
                },
                resolve: {
                    activeGameCategorySlug: ['$stateParams', function($stateParams){
                        return $stateParams.gameCategorySlug;
                    }]
                }
            });
    }

})();
