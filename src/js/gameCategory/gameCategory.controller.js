;(function(){
    'use strict';

    angular
        .module('app.gameCategory')
        .controller('GameCategoryController', GameCategoryController);

    GameCategoryController.$inject = ['activeGameCategorySlug', 'gameCategoriesService'];

    function GameCategoryController(activeGameCategorySlug, gameCategoriesService){
        var vm = this;

        // Attaching injected dependencies to the instance
        vm.activeGameCategorySlug = activeGameCategorySlug;
        vm.gameCategoriesService = gameCategoriesService;

        // Fetch games from the active category
        vm.gameCategoriesService.getBySlug(vm.activeGameCategorySlug)
            .then(function(gameCategory){
                vm.games = gameCategory._embedded.games;
            });
    }

})();
